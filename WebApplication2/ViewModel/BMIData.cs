﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.ViewModel
{
    public class BMIData
    {
        public float Weight { get; set; }
        public float Height { get; set; }
        public float BMI { get; set; }
        public string Level { get; set; }
    }
}